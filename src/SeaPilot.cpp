#include "SeaPilot.h"
#include <string>
#include <cmath>
#include <cstdio>

SeaPilot::SeaPilot(string pathLogFile, bool enableLog)
    : BaseDriver(pathLogFile)
{
    
    enableLogging(enableLog);
    
#ifdef __DEBUG_FLAG_
        cout << "Seapilot :: log file :" << pathLogFile <<" , actived :" << enableLog << std::endl;
#endif
    
    // init datas
    _time = 0;
    _sampleNumber = 0;
    
    // water temperature
    _temperature = 0;
    
    // pressure
    _pressure = 0;
    
    //bottom speed
    _bottomTrackXVelocity = 0;
    _bottomTrackYVelocity = 0;
    _bottomTrackZVelocity = 0;
    _bottomTrackEastVelocity = 0;
    _bottomTrackNorthVelocity = 0;
    _bottomTrackUpVelocity = 0;
    //distance depth
    _depthBottom = 0;
    
    //water speed
    _waterMassXVelocity = 0;
    _waterMassYVelocity = 0;
    _waterMassZVelocity = 0;
    _waterMassEastVelocity = 0;
    _waterMassNorthVelocity = 0;
    _waterMassUpVelocity = 0;
    //distance water
    _depthWaterMass = 0;
    
    //attitude for the water tracking
    _rollUsedForWaterTracking = 0;
    _pitchUsedForWaterTracking = 0;
    _yawUsedForWaterTracking = 0;
    
    // attittude dor the bottom tracking
    _rollUsedForBottomTracking = 0;
    _pitchUsedForBottomTracking = 0;
    _yawUsedForBottomTracking = 0;
    
    // bit status
    _status = 0x0000;
    
    // nombre de données invalides consécutives
    _counterInvalidBT = 0;
    _counterInvalidWT = 0;
    
    //delta de temps entre deux données
    _dt = 0.0;

    _sync = false;
}

SeaPilot::~SeaPilot() {
    stop();
    disconnect();
    
}

bool SeaPilot::help() {
    unsigned char reqbuf[] = {'?',MESSAGE_TERMINATOR_SEAPILOT};   
    return write(reqbuf,sizeof(reqbuf));
}

bool SeaPilot::start() {
    _sync = false;
    unsigned char reqbuf[] = {'S', 'T', 'A', 'R', 'T',MESSAGE_TERMINATOR_SEAPILOT};
    return write(reqbuf,sizeof(reqbuf));

}

bool SeaPilot::startOneShot(){
    _sync = false;
    unsigned char reqbuf[] = {'S', 'T', 'A', 'R', 'T','M','E',MESSAGE_TERMINATOR_SEAPILOT};
    return write(reqbuf,sizeof(reqbuf));
}

bool SeaPilot::stop() {
    _sync = false;
    unsigned char reqbuf[] = {'S', 'T', 'O', 'P', MESSAGE_TERMINATOR_SEAPILOT};
    return write(reqbuf,sizeof(reqbuf));
}

bool SeaPilot::sleep() {
    _sync = false;
    unsigned char reqbuf[] = {'S', 'L', 'E', 'E', 'P', MESSAGE_TERMINATOR_SEAPILOT};
    return write(reqbuf,sizeof(reqbuf));
}

string SeaPilot::waitDatas(){
    return readDatasToString('\n');
}

int SeaPilot::readOnSensor(string trame){
    
//********************************    
    //TODO gestion du CRC CCITT - 16 bits à gerer 
    
    //exemple de trame
    //trame = "$PRTI01,480,29,2752,-99999,-99999,-99999,6561,,,,,0000,3,0*38\n";
    //$PRTI01,490,30,2752,-99999,-99999,-99999,0,,,,,0044,3,0*05
    //$PRTI02,490,30,2752,-99999,-99999,-99999,0,,,,,0044,3,0*06
    //$PRTI02,500,31,2752,-99999,-99999,-99999,0,,,,,0044,3,0*0F
    //trame = "$PRTI30,59.884,-69.109,-84.269,3,0";//*23

    //cout << "checksum : " << hex <<calculateChecksum(trame.c_str(),trame.size()) << endl;

//*******************************
    
#ifdef __DEBUG_FLAG_
        cout << trame << std::endl;
#endif
        

    string id = trame.substr(0, trame.find(","));
        trame.erase(0, trame.find(",") + 1);
     
    if (id == "$PRTI01") _sync = true;
        
    if (id == "$PRTI01" && _sync == true) {
     
        //si le log est activer on print la trame antérieur
        printToLogFile(getDatas());
        
#ifdef __DEBUG_FLAG_
        cout << "Trame PRTI01" << std::endl;
        cout << trame.substr(0, trame.find(",")) << std::endl;
#endif
        

        if (trame.substr(0, trame.find(",")) != "") {
            int tmp = _time;
            _time = stoi(trame.substr(0, trame.find(",")));
            
            //calcul de dt
            _dt = (_time - tmp)/100.0;
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _sampleNumber = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _temperature = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _bottomTrackXVelocity = stoi(trame.substr(0, trame.find(",")));
            if(_bottomTrackXVelocity <= -99999){
                _btIsValid = false;
                _counterInvalidBT++;
            }else{
                _btIsValid = true;
                _counterInvalidBT = 0;
            }
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _bottomTrackYVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _bottomTrackZVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _depthBottom = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _waterMassXVelocity = stoi(trame.substr(0, trame.find(",")));
            if(_waterMassXVelocity <= -99999){
                _wtIsValid = false;
                _counterInvalidWT++;
            }else{
                _wtIsValid = true;
                _counterInvalidWT = 0;
            }
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _waterMassYVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _waterMassZVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _depthWaterMass = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);
        
        if (trame.substr(0, trame.find(",")) != "") {
            _status = stoi(trame.substr(0, trame.find(",")),0,16);
        }
        trame.erase(0, trame.find(",") + 1);
        
        //passage des vitesse dans le repère de l'engin
        Vector3D bottomSpeedLocal = frameshiftLocal(_bottomTrackXVelocity,_bottomTrackYVelocity,_bottomTrackZVelocity);
        _bottomTrackXVelocity = bottomSpeedLocal.x;
        _bottomTrackYVelocity = bottomSpeedLocal.y;
        _bottomTrackZVelocity = bottomSpeedLocal.z;
        //passage des vitesse dans le repère de l'engin
        Vector3D waterSpeedLocal = frameshiftLocal(_waterMassXVelocity,_waterMassYVelocity,_waterMassZVelocity);
        _waterMassXVelocity = waterSpeedLocal.x;
        _waterMassYVelocity = waterSpeedLocal.y;
        _waterMassZVelocity = waterSpeedLocal.z;

    } else if (id == "$PRTI02" && _sync == true) {

#ifdef __DEBUG_FLAG_
        cout << "Trame PRTI02" << std::endl;
        cout << trame.substr(0, trame.find(",")) << std::endl;
#endif
        

        if (trame.substr(0, trame.find(",")) != "") {
            _time = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _sampleNumber = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _temperature = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _bottomTrackEastVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _bottomTrackNorthVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _bottomTrackUpVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _depthBottom = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _waterMassEastVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _waterMassNorthVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _waterMassUpVelocity = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _depthWaterMass = stoi(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);
        
        if (trame.substr(0, trame.find(",")) != "") {
            _status = stoi(trame.substr(0, trame.find(",")),0,16);
        }
        trame.erase(0, trame.find(",") + 1);
        
        //passage des vitesse dans le repère de l'engin
        Vector3D bottomSpeedLocal = frameshiftLocal(_bottomTrackXVelocity,_bottomTrackYVelocity,_bottomTrackZVelocity);
        _bottomTrackXVelocity = bottomSpeedLocal.x;
        _bottomTrackYVelocity = bottomSpeedLocal.y;
        _bottomTrackZVelocity = bottomSpeedLocal.z;
        //passage des vitesse dans le repère de l'engin
        Vector3D waterSpeedLocal = frameshiftLocal(_waterMassXVelocity,_waterMassYVelocity,_waterMassZVelocity);
        _waterMassXVelocity = waterSpeedLocal.x;
        _waterMassYVelocity = waterSpeedLocal.y;
        _waterMassZVelocity = waterSpeedLocal.z;

    } else if (id == "$PRTI30" && _sync == true) {
#ifdef __DEBUG_FLAG_
        cout << "Trame PRTI30" << std::endl;
#endif

        if (trame.substr(0, trame.find(",")) != "") {
            _yawUsedForBottomTracking = stof(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _pitchUsedForBottomTracking = stof(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _rollUsedForBottomTracking = stof(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        NormalizedAngle angle = normalizeAngleLocal(_rollUsedForBottomTracking,_pitchUsedForBottomTracking,_yawUsedForBottomTracking);
        _rollUsedForBottomTracking = angle.phi;
        _pitchUsedForBottomTracking = angle.theta;
        _yawUsedForBottomTracking = angle.psi;

    //
    }else if (id == "$PRTI31" && _sync == true) {
#ifdef __DEBUG_FLAG_
        cout << "Trame PRTI31" << std::endl;
#endif

        if (trame.substr(0, trame.find(",")) != "") {
            _yawUsedForWaterTracking = stof(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _pitchUsedForWaterTracking = stof(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);

        if (trame.substr(0, trame.find(",")) != "") {
            _rollUsedForWaterTracking = stof(trame.substr(0, trame.find(",")));
        }
        trame.erase(0, trame.find(",") + 1);
        
        NormalizedAngle angle = normalizeAngleLocal(_rollUsedForWaterTracking,_pitchUsedForWaterTracking,_yawUsedForWaterTracking);
        _rollUsedForWaterTracking = angle.phi;
        _pitchUsedForWaterTracking = angle.theta;
        _yawUsedForWaterTracking = angle.psi;

    }else{
        cout << trame << std::endl;
        return -1;
    }
    return 0;
}

int SeaPilot::readOnFile(string trame){
    
#ifdef __DEBUG_FLAG_
    cout << "readOnFile :: trame : " << trame << std::endl;
#endif
    if (trame.substr(0, trame.find(";")) != "") {
        int tmp = _time;
        _time = stoi(trame.substr(0, trame.find(";")));
        //calcul de dt
        _dt = (_time - tmp)/100.0;
    }
    trame.erase(0, trame.find(";") + 1);
    
    if (trame.substr(0, trame.find(";")) != "") {
        _sampleNumber = stoi(trame.substr(0, trame.find(";")));

    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _temperature = stof(trame.substr(0, trame.find(";"))) * 100;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _bottomTrackXVelocity = stof(trame.substr(0, trame.find(";")))*1000;
        if(_bottomTrackXVelocity <= -99999){
            _btIsValid = false;
            _counterInvalidBT++;
        }else{
            _btIsValid = true;
            _counterInvalidBT = 0;
        }
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _bottomTrackYVelocity = stof(trame.substr(0, trame.find(";")))*1000;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _bottomTrackZVelocity = stof(trame.substr(0, trame.find(";")))*1000;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _depthBottom = stof(trame.substr(0, trame.find(";")))*1000;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _waterMassXVelocity = stof(trame.substr(0, trame.find(";")))*1000;
        if(_waterMassXVelocity <= -99999){
            _wtIsValid = false;
            _counterInvalidWT++;
        }else{
            _wtIsValid = true;
            _counterInvalidWT = 0;
        }
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _waterMassYVelocity = stof(trame.substr(0, trame.find(";")))*1000;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _waterMassZVelocity = stof(trame.substr(0, trame.find(";")))*1000;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _depthWaterMass = stof(trame.substr(0, trame.find(";")))*1000;
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _status = stoi(trame.substr(0, trame.find(";")),0,16);
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _yawUsedForBottomTracking = stof(trame.substr(0, trame.find(";")));
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _pitchUsedForBottomTracking = stof(trame.substr(0, trame.find(";")));
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _rollUsedForBottomTracking = stof(trame.substr(0, trame.find(";")));
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _yawUsedForWaterTracking = stof(trame.substr(0, trame.find(";")));
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _pitchUsedForWaterTracking = stof(trame.substr(0, trame.find(";")));
    }
    trame.erase(0, trame.find(";") + 1);

    if (trame.substr(0, trame.find(";")) != "") {
        _rollUsedForWaterTracking = stof(trame.substr(0, trame.find(";")));
    }
    trame.erase(0, trame.find(";") + 1);
    
    //passage des vitesse dans le repère de l'engin
    Vector3D bottomSpeedLocal = frameshiftLocal(_bottomTrackXVelocity,_bottomTrackYVelocity,_bottomTrackZVelocity);
    _bottomTrackXVelocity = bottomSpeedLocal.x;
    _bottomTrackYVelocity = bottomSpeedLocal.y;
    _bottomTrackZVelocity = bottomSpeedLocal.z;
    //passage des vitesse dans le repère de l'engin
    Vector3D waterSpeedLocal = frameshiftLocal(_waterMassXVelocity,_waterMassYVelocity,_waterMassZVelocity);
    _waterMassXVelocity = waterSpeedLocal.x;
    _waterMassYVelocity = waterSpeedLocal.y;
    _waterMassZVelocity = waterSpeedLocal.z;

    return 0;
}

void SeaPilot::getStatusDVL(int * statusActived , int &size){
    size = 0;
    for(int i = 0 ; i < 16 ; i++){
        //permet de récupérer chacune des erreurs
        if( (_status & (1 << i))) {
            statusActived[size] = pow(2,i);
            size += 1;
        }
    }
}

int SeaPilot::getBottomTrackXVelocity() const {
    return _bottomTrackXVelocity;
}

int SeaPilot::getBottomTrackYVelocity() const {
    return _bottomTrackYVelocity;
}

int SeaPilot::getBottomTrackZVelocity() const {
    return _bottomTrackZVelocity;
}

int SeaPilot::getBottomTrackEastVelocity() const {
    return _bottomTrackEastVelocity;
}

int SeaPilot::getBottomTrackNorthVelocity() const {
    return _bottomTrackNorthVelocity;
}

int SeaPilot::getBottomTrackUpVelocity() const {
    return _bottomTrackUpVelocity;
}

int SeaPilot::getDepthBottom() const {
    return _depthBottom;
}

int SeaPilot::getDepthWaterMass() const {
    return _depthWaterMass;
}

unsigned int SeaPilot::getSampleNumber() const {
    return _sampleNumber;
}

int SeaPilot::getTemperature() const {
    return _temperature;
}

unsigned int SeaPilot::getTime() const {
    return _time;
}

int SeaPilot::getWaterMassXVelocity() const {
    return _waterMassXVelocity;
}

int SeaPilot::getWaterMassYVelocity() const {
    return _waterMassYVelocity;
}

int SeaPilot::getWaterMassZVelocity() const {
    return _waterMassZVelocity;
}

int SeaPilot::getWaterMassEastVelocity() const {
    return _waterMassEastVelocity;
}

int SeaPilot::getWaterMassNorthVelocity() const {
    return _waterMassNorthVelocity;
}

int SeaPilot::getWaterMassUpVelocity() const {
    return _waterMassUpVelocity;
}

float SeaPilot::getRollUsedForWaterTracking() const {
    return _rollUsedForWaterTracking;
}

float SeaPilot::getPitchUsedForWaterTracking() const {
    return _pitchUsedForWaterTracking;
}

float SeaPilot::getYawUsedForWaterTracking() const {
    return _yawUsedForWaterTracking;
}

float SeaPilot::getRollUsedForBottomTracking() const {
    return _rollUsedForBottomTracking;
}

float SeaPilot::getPitchUsedForBottomTracking() const {
    return _pitchUsedForBottomTracking;
}

float SeaPilot::getYawUsedForBottomTracking() const {
    return _yawUsedForBottomTracking;
}
///
float SeaPilot::getRollToRadUsedForWaterTracking() const {
    return (_rollUsedForWaterTracking*PI)/180;
}

float SeaPilot::getPitchToRadUsedForWaterTracking() const {
    return (_pitchUsedForWaterTracking*PI)/180;
}

float SeaPilot::getYawToRadUsedForWaterTracking() const {
    return (_yawUsedForWaterTracking*PI)/180;
}

float SeaPilot::getRollToRadUsedForBottomTracking() const {
    return (_rollUsedForBottomTracking*PI)/180;
}

float SeaPilot::getPitchToRadUsedForBottomTracking() const {
    return (_pitchUsedForBottomTracking*PI)/180;
}

float SeaPilot::getYawToRadUsedForBottomTracking() const {
    return (_yawUsedForBottomTracking*PI)/180;
}

void SeaPilot::setOutputRate(int h, int m, int s, int ms) {
    char *hour , *minute ,*second , *millisecond ;
    asprintf(&hour,"%d",h);
    asprintf(&minute,"%d",m);
    asprintf(&second,"%d",s);
    asprintf(&millisecond,"%d",ms);
    
    string output = "CEI ";
    if(h >= 10){
        output += hour;
    }else{
        output += "0";
        output += hour;
    }
    output += ":";
    if(m >= 10){
        output += minute;
    }else{
        output += "0";
        output += minute;
    }
    output += ":";
    if(s >= 10){
        output += second;
    }else{
        output += "0";
        output += second;
    }
    output += ".";
    if(ms >= 10){
        output += millisecond;
    }else{
        output += "0";
        output += millisecond;
    } 
    output += "\r";
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(hour); 
    free(minute);
    free(second);
    free(millisecond) ;
}

void SeaPilot::setEnableBottomTrack(bool active){    
    string output = "CBTON ";
    
    if(active == true) output += "1";
    else output += "0";
    
    output += "\r";
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
}

void SeaPilot::setWaterSalinity(double ppt){
    char *salinity;
    asprintf(&salinity,"%2.f",ppt);
    
    string output = "CWS ";
    output += salinity;
    
    output += "\r";
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(salinity);
}

bool SeaPilot::setRealTime(int yyyy , int mm , int dd, int h, int m, int s ){
    char *year, *month, *day, *hour , *minute ,*second;
    asprintf(&year,"%d",yyyy);
    asprintf(&month,"%d",mm);
    asprintf(&day,"%d",dd);
    asprintf(&hour,"%d",h);
    asprintf(&minute,"%d",m);
    asprintf(&second,"%d",s);

    string output = "STIME ";
    
    if(yyyy <= 9999 && yyyy >= 2015){
        output += yyyy;
    }else {
        return false;
    }
    output += "/";
    if(mm <= 12 && mm >= 1){
        if(mm <= 9)output += "0";
        output += mm;
    }else{
        return false;
    }
    output += "/";
    if(dd <= 31 && dd >= 1){
        if(dd <= 9)output += "0";
        output += dd;
        
    }else{
        return false;
    }
    
    output += ",";
    
    if(h >= 10){
        output += hour;
    }else{
        output += "0";
        output += hour;
    }
    output += ":";
    if(m >= 10){
        output += minute;
    }else{
        output += "0";
        output += minute;
    }
    output += ":";
    if(s >= 10){
        output += second;
    }else{
        output += "0";
        output += second;
    }

    output += "\r";
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(year);
    free(month);
    free(day);
    free(hour);
    free(minute);
    free(second);
    
    return true;
}

char* SeaPilot::numberToArray(int number) {
    int n = log10(number)+1;
    char *numberArray = (char*) calloc(n,sizeof(char));
    for(int i = 0 ; i < n ;  i++,number /=10){
        numberArray[i] = number % 10;
    }
    return numberArray;
}

int SeaPilot::calculateChecksum(const char* buf, int size){
    
    long ensembleSize = buf[24];
    ensembleSize += buf[25] << 8;
    ensembleSize += buf[26] << 16;
    ensembleSize += buf[27] << 24;
    
    ushort crc = 0;
    for( int i = 32 ; i < ensembleSize + 32 ; i++){
        crc = (ushort)((char)(crc>>8) | (crc << 8));
        crc ^= buf[i];
        crc ^= (char)((crc & 0xff) >> 4) ;
        crc ^= (ushort)((crc << 8) << 4);
        crc ^= (ushort)(((crc & 0xff) << 4)<<1);
    }
    return (ushort) crc;
    
    
}

void SeaPilot::setBottomTrackControl(BottomTrackControl_mode mode, double pulseToPulse, double longRangeDepth, BottomTrackControl_BeamMultiplex beamMultiplex){
    
    char * c_mode, *c_pulseToPulse,*c_longRangeDepth,* c_beamMultiplex ;
    
    asprintf(&c_mode,"%d",mode);
    asprintf(&c_pulseToPulse,"%.1f",pulseToPulse);
    asprintf(&c_longRangeDepth,"%.1f",longRangeDepth);
    asprintf(&c_beamMultiplex,"%d",beamMultiplex);
    
    string output = "CBTBB ";
    output += c_mode;
    output += ",";
    output += c_pulseToPulse;
    output += ",";
    output += c_longRangeDepth;
    output += ",";
    output += c_beamMultiplex;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setBottomTrackControl: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_mode);
    free(c_pulseToPulse);
    free(c_longRangeDepth);
    free(c_beamMultiplex);
}

void SeaPilot::setBTSreeningThresholds(double correlationThreshold, double Q_VelocityThreshold , double V_VelocityThreshold){
    char * c_correlationThreshold, *c_Q_VelocityThreshold,*c_V_VelocityThreshold;
    
    asprintf(&c_correlationThreshold,"%.2f",correlationThreshold);
    asprintf(&c_Q_VelocityThreshold,"%.2f",Q_VelocityThreshold);
    asprintf(&c_V_VelocityThreshold,"%.2f",V_VelocityThreshold);
    
    string output = "CBTST ";
    output += c_correlationThreshold;
    output += ",";
    output += c_Q_VelocityThreshold;
    output += ",";
    output += c_V_VelocityThreshold;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setBottomTrackSreeningThresholds: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_correlationThreshold);
    free(c_Q_VelocityThreshold);
    free(c_V_VelocityThreshold);
}

void SeaPilot::setBTThresholds(double SNR_shallow ,double depth_shallow, double SNR_deep ,double depth_deep){
    char * c_SNR_shallow, *c_depth_shallow, *c_SNR_deep, *c_depth_deep;
    
    asprintf(&c_SNR_shallow,"%.2f",SNR_shallow);
    asprintf(&c_depth_shallow,"%.2f",depth_shallow);
    asprintf(&c_SNR_deep,"%.2f",SNR_deep);
    asprintf(&c_depth_deep,"%.2f",depth_deep);
    
    string output = "CBTT ";
    output += c_SNR_shallow;
    output += ",";
    output += c_depth_shallow;
    output += ",";
    output += c_SNR_deep;
    output += ",";
    output += c_depth_deep;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setBottomTrackThresholds: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_SNR_shallow);
    free(c_depth_shallow);
    free(c_SNR_deep);
    free(c_depth_deep);
}

void SeaPilot::setBottomTrackBlank(double blank){
    char * c_blank;
    
    asprintf(&c_blank,"%.2f",blank);;
    
    string output = "CBTBL ";
    output += c_blank;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setBottomTrackBlank: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_blank);
}

void SeaPilot::setBottomTrackMaxDepth(double maxDepth){
    char * c_maxDepth;
    
    asprintf(&c_maxDepth,"%.2f",maxDepth);;
    
    string output = "CBTMX ";
    output += c_maxDepth;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setBottomTrackMaxDepth: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_maxDepth);
}

void SeaPilot::setBTTimeBetweenPings(double time){
    char * c_time;
    
    asprintf(&c_time,"%.2f",time);;
    
    string output = "CBTTBP ";
    output += c_time;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setTimeBetweenPings: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_time);
}

void SeaPilot::setBottomTrackFilter( double output_timeConstant, double ref_timeConstant , int ref_outlierCount , double ref_outlierThreshold, int output_coastCount){
    char * c_output_timeConstant, *c_ref_timeConstant,*c_ref_outlierCount,* c_ref_outlierThreshold, *c_output_coastCount ;
    
    asprintf(&c_output_timeConstant,"%.2f",output_timeConstant);
    asprintf(&c_ref_timeConstant,"%.2f",ref_timeConstant);
    asprintf(&c_ref_outlierCount,"%d",ref_outlierCount);
    asprintf(&c_ref_outlierThreshold,"%.2f",ref_outlierThreshold);
    asprintf(&c_output_coastCount,"%d",output_coastCount);
    
    string output = "CBTFILT ";
    output += c_output_timeConstant;
    output += ",";
    output += c_ref_timeConstant;
    output += ",";
    output += c_ref_outlierCount;
    output += ",";
    output += c_ref_outlierThreshold;
    output += ",";
    output += c_output_coastCount;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setBottomTrackFilter: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_output_timeConstant);
    free(c_ref_timeConstant);
    free(c_ref_outlierCount);
    free(c_ref_outlierThreshold);
    free(c_output_coastCount);
    
}

void SeaPilot::setEnableWaterTracking(bool enable, double min_rangeProfiling, double max_rangeProfiling){
    char * c_enable, *c_min_rangeProfiling,*c_max_rangeProfiling;
    
    asprintf(&c_enable,"%d",enable);
    asprintf(&c_min_rangeProfiling,"%.2f",min_rangeProfiling);
    asprintf(&c_max_rangeProfiling,"%.2f",max_rangeProfiling);
    
    string output = "CWTON ";
    output += c_enable;
    output += ",";
    output += c_min_rangeProfiling;
    output += ",";
    output += c_max_rangeProfiling;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setEnableWaterTracking: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_enable);
    free(c_min_rangeProfiling);
    free(c_max_rangeProfiling);
}

void SeaPilot::setEnableWTBroadband(bool enable){
    char * c_enable;
    
    asprintf(&c_enable,"%d",enable);
    
    string output = "CWTBB ";
    output += c_enable;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setEnableWaterTrackingBroadband: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_enable);
}

void SeaPilot::setWTBlank(double blank){
    char * c_blank;
    
    asprintf(&c_blank,"%.2f",blank);
    
    string output = "CWTBL ";
    output += c_blank;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setWaterTrackingBlank: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_blank);
}

void SeaPilot::setWTBinSize(double binSize){
    char * c_binSize;
    
    asprintf(&c_binSize,"%.2f",binSize);
    
    string output = "CWTBS ";
    output += c_binSize;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setWaterTrackingBinSize: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_binSize);
}

void SeaPilot::setWTTimeBetweenPings(double time){
    char * c_time;
    
    asprintf(&c_time,"%.2f",time);;
    
    string output = "CWTTBP ";
    output += c_time;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setWTTimeBetweenPings: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_time);
}

void SeaPilot::setWaterSpeedOfSound(WaterSpeedOfSound_ctrl waterTemperature_src , WaterSpeedOfSound_ctrl transducerDepth_src , WaterSpeedOfSound_ctrl salinity_src , WaterSpeedOfSound_ctrl speedOfSound_src){
    char * c_waterTemperature_src,*c_transducerDepth_src,* c_salinity_src, * c_speedOfSound_src ;
    
    asprintf(&c_waterTemperature_src,"%d",waterTemperature_src);
    asprintf(&c_transducerDepth_src,"%d",transducerDepth_src);
    asprintf(&c_salinity_src,"%d",salinity_src);
    asprintf(&c_speedOfSound_src,"%d",speedOfSound_src);
    
    string output = "CWSSC ";
    output += c_waterTemperature_src;
    output += ",";
    output += c_transducerDepth_src;
    output += ",";
    output += c_salinity_src;
    output += ",";
    output += c_speedOfSound_src;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setWaterSpeedOfSound: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_waterTemperature_src);
    free(c_transducerDepth_src);
    free(c_salinity_src);
    free(c_speedOfSound_src);
    
}

void SeaPilot::setWaterTemperature(double waterTemperature){
    char * c_waterTemperature;
    
    asprintf(&c_waterTemperature,"%.2f",waterTemperature);
    
    string output = "CWT ";
    output += c_waterTemperature;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setWaterTemperature: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_waterTemperature);
}

void SeaPilot::setTransducerDepth(double depth){
    char * c_depth;
    
    asprintf(&c_depth,"%.2f",depth);
    
    string output = "CTD ";
    output += c_depth;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setTransducerDepth: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_depth);
}

void SeaPilot::setWaterSpeedOfSound(double waterSpeedOfSound){
    
    char * c_waterSpeedOfSound;
    
    asprintf(&c_waterSpeedOfSound,"%.2f",waterSpeedOfSound);
    
    string output = "CWSS ";
    output += c_waterSpeedOfSound;
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setWaterSpeedOfSound: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    free(c_waterSpeedOfSound);
}

void SeaPilot::setZeroPressureSensor(){
    string output = "CPZ";
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "setZeroPressureSensor: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
}

void SeaPilot::loadConfiguration(){
    string output = "CLOAD";
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "loadConfiguration: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
}
    
void SeaPilot::saveConfiguration(){
    string output = "CSAVE";
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "loadConfiguration: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
}

void SeaPilot::loadDefaultConfiguration(){
    string output = "CDEFAULT";
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "loadConfiguration: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
}

string SeaPilot::showConfiguration(){
    
    string output = "CSHOW";
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "showConfiguration: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    char recvbuf[1];
    char tmp;
    string trame = "";
    bool serialDetected = false;

    do {

        tmp = recvbuf[0];
        
        //lecture avec un timeout de 10s
        if (readDatas(recvbuf, 1,TIMEOUT_SEAPILOT) < 0) {
#ifdef __DEBUG_FLAG_
            cout << "Error reading data from the device." << std::endl;
#endif
            return "Error or Timeout";
        }
        trame += recvbuf[0];

        //std::cout << recvbuf[0] <<std::endl;
        
        if(recvbuf[0] == 'N' && tmp == 'S')serialDetected = true;

        // permet de détecter la présence de deux retour charriot 
    } while (recvbuf[0] != '\n' || serialDetected != true);
    
    return trame;
}

string SeaPilot::getDatas() {

    //timestamp; N° échantillon ; Température ; vitesse fond X ; vitesse fond Y ; vitesse fond Z ; altitude ; vitesse eau X ; vitesse eau Y ; vitesse eau Z ; profondeur ; status ; cap (bottom tracking) ; roulis (bottom tracking); tangage (bottom tracking) ; cap (water mass tracking) ; roulis (water mass tracking); tangage (water mass tracking)
    
    char * c_time ,* c_time_dvl , * c_sample, *c_temperature, *c_speed_bottom_x, *c_speed_bottom_y,*c_speed_bottom_z , *c_altitude,*c_speed_water_x,*c_speed_water_y,*c_speed_water_z, *c_depth , *c_status,*c_bottom_yaw,*c_bottom_pitch,*c_bottom_roll,*c_water_yaw,*c_water_pitch,*c_water_roll ;
    
    asprintf(&c_time,"%lu",getTimeSystem());
    asprintf(&c_time_dvl,"%d",getTime());
    asprintf(&c_sample,"%d",getSampleNumber());
    asprintf(&c_temperature,"%.2f",getTemperature()/100.0); // température en degrée
    asprintf(&c_speed_bottom_x,"%.3f",getBottomTrackXVelocity()/1000.0); // vitesse en m/s
    asprintf(&c_speed_bottom_y,"%.3f",getBottomTrackYVelocity()/1000.0); // vitesse en m/s
    asprintf(&c_speed_bottom_z,"%.3f",getBottomTrackZVelocity()/1000.0); // vitesse en m/s
    asprintf(&c_altitude,"%.3f",getDepthBottom()/1000.0); // altitude en m
    asprintf(&c_speed_water_x,"%.3f",getWaterMassXVelocity()/1000.0); // vitesse en m/s
    asprintf(&c_speed_water_y,"%.3f",getWaterMassYVelocity()/1000.0); // vitesse en m/s
    asprintf(&c_speed_water_z,"%.3f",getWaterMassZVelocity()/1000.0); // vitesse en m/s
    asprintf(&c_depth,"%.3f",getDepthWaterMass()/1000.0); // profondeur en m/s
    asprintf(&c_status,"0x%x",_status);
    asprintf(&c_bottom_yaw,"%.3f",getYawUsedForBottomTracking()); // cap pour le bottom tracking
    asprintf(&c_bottom_pitch,"%.3f",getPitchUsedForBottomTracking()); // roulis pour le bottom tracking
    asprintf(&c_bottom_roll,"%.3f",getRollUsedForBottomTracking()); // tangage pour le bottom tracking
    asprintf(&c_water_yaw,"%.3f",getYawUsedForWaterTracking()); // cap pour le water tracking
    asprintf(&c_water_pitch,"%.3f",getPitchUsedForWaterTracking()); // roulis pour le water tracking
    asprintf(&c_water_roll,"%.3f",getRollUsedForWaterTracking()); // tangage pour le water tracking
    
    string output = c_time;
    output += ";";
    output += c_time_dvl ;
    output += ";";
    output += c_sample;
    output += ";";
    output += c_temperature;
    output += ";";
    output += c_speed_bottom_x;
    output += ";";
    output += c_speed_bottom_y;
    output += ";";
    output += c_speed_bottom_z ;
    output += ";";
    output += c_altitude;
    output += ";";
    output += c_speed_water_x;
    output += ";";
    output += c_speed_water_y;
    output += ";";
    output += c_speed_water_z;
    output += ";";
    output += c_depth;
    output += ";";
    output += c_status;
    output += ";";
    output += c_bottom_yaw;
    output += ";";
    output += c_bottom_pitch;
    output += ";";
    output += c_bottom_roll;
    output += ";";
    output += c_water_yaw;
    output += ";";
    output += c_water_pitch;
    output += ";";
    output += c_water_roll;
    output += "\n";
    
#ifdef __DEBUG_FLAG_
        cout << "Print log "<< output << std::endl;
#endif
        
    free(c_time);
    free(c_time_dvl);
    free(c_sample);
    free(c_temperature);
    free(c_speed_bottom_x);
    free(c_speed_bottom_y);
    free(c_speed_bottom_z);
    free(c_altitude);
    free(c_speed_water_x);
    free(c_speed_water_y);
    free(c_speed_water_z);
    free(c_depth);
    free(c_status);
    free(c_bottom_yaw);
    free(c_bottom_pitch);
    free(c_bottom_roll);
    free(c_water_yaw);
    free(c_water_pitch);
    free(c_water_roll);

    return output;
    
    
}

string SeaPilot::diagnoticCompass(){
    string output = "DIAGPNI 1";
    output += "\r";
    
#ifdef __DEBUG_FLAG_
    cout << "diagnoticCompass: " << output << std::endl;
#endif
    
    unsigned char * out = (unsigned char *) output.c_str();
    write( out,output.size());
    
    return waitDatas();
}

SeaPilot::dvlConfiguration SeaPilot::getConfiguration(){
    
    dvlConfiguration conf;
    
    string trame = showConfiguration();

    string splitTrame[100];
    int sizeTabSplitTrame = 0;
    string tmp = "";
    
    // on parse la trame, et on ajoute chaque ligne dans un tableau
    for(int i = 0; i < trame.length();i++){
        
        // on supprime les [0] des trames
        if(trame.at(i) == '[' ) i += 3;
        
        tmp += trame.at(i);
        //cout << trame.at(i) << endl;
        
        if( trame.at(i) == '\n'){
            splitTrame[sizeTabSplitTrame].append(tmp);
            sizeTabSplitTrame++;
            tmp = "";
        }
    }
    
    // parcours du tableau de string
    for(int i = 0; i < sizeTabSplitTrame;i++){
        
        #ifdef __DEBUG_FLAG_
            cout << "config :" <<splitTrame[i] << std::endl;
        #endif

        // suppression double espace
        splitTrame[i].erase(0, splitTrame[i].find("  ") + 2);
        
        
        if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CEI"){

            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);
            
            // récupération des valeurs;
            conf.cei.hour = stoi(splitTrame[i].substr(0, splitTrame[i].find(":")));
            splitTrame[i].erase(0, splitTrame[i].find(":") + 1);
            conf.cei.minute = stoi(splitTrame[i].substr(0, splitTrame[i].find(":")));
            splitTrame[i].erase(0, splitTrame[i].find(":") + 1);
            conf.cei.seconde = stof(splitTrame[i].substr(0, splitTrame[i].find(":")));
            splitTrame[i].erase(0, splitTrame[i].find(":") + 1);
            conf.cei.ms = stoi(splitTrame[i].substr(0, splitTrame[i].find(".")));


        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTON"){
                // suppression de l'entete
                splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);
                // récupération de la première valeur
                conf.cbton = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));

        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTBB"){
            
             // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cbtbb.mode = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtbb.p2pLag= stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtbb.longRangeDepth = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtbb.beamMultiplex = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTST"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cbtst.correlationThreshold = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtst.qVelocityThreshold= stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtst.vVelocityThreshold = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTBL"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cbtbl = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));

        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTMX"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cbtmx = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTTBP"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cbttbp = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));

        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTT"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cbtt.shallowDetectionThreshold = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtt.depthForShallowDetection = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtt.deepDetectionThreshold = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtt.depth = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));

        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CBTFILT"){
             // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);
            

            // récupération des valeurs;
            conf.cbtfilt.outputLowPassFilter = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtfilt.refLowPassFilter = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtfilt.refOutlierCount = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtfilt.refOutlierThreshold = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cbtfilt.outputCoastCount = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
            
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWTON"){
             // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwton.enable = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cwton.minRange = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cwton.maxRange = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWTBB"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwtbb = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWTBL"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwtbl = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWTBS"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwtbs = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWTTBP"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwttbp = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWSSC"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwssc.waterTemperatureSrc = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cwssc.transducerDepthSrc = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cwssc.salinitySrc = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            splitTrame[i].erase(0, splitTrame[i].find(",") + 1);
            conf.cwssc.speedOfSoundSrc = stoi(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWS"){
             // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cws = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWT"){
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwt = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CTD"){
        
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.ctd = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
            
        }else if (splitTrame[i].substr(0, splitTrame[i].find(" ")) == "CWSS"){
        
            // suppression de l'entete
            splitTrame[i].erase(0, splitTrame[i].find(" ") + 1);

            // récupération des valeurs;
            conf.cwss = stof(splitTrame[i].substr(0, splitTrame[i].find(",")));
        }

    }
    
    return conf;

}

bool SeaPilot::bottomTrackingIsValid(){
    return _btIsValid;
}

bool SeaPilot::waterTrackingIsValid(){
    return _wtIsValid;
}

unsigned int SeaPilot::numberOfBottomTrackInvalid(){
    return _counterInvalidBT;
}

unsigned int SeaPilot::numberOfWaterTrackInvalid(){
    return _counterInvalidWT;
}

float SeaPilot::getDt(){
    return _dt;
}