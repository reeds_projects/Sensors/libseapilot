#ifndef __SEAPILOT_H_
#define	__SEAPILOT_H_

#include <iostream>
#include <string>

#include "BaseDriver.h"

#define PI 3.14159265359

//permet d'activer l'affichage du debug
//#define __DEBUG_FLAG_

#define	SEAPILOT_RECVBUFFER_SIZE 2048
#define MESSAGE_TERMINATOR_SEAPILOT 0x0d // CR.
#define MESSAGE_DELIMITER_SEAPILOT ',' 
#define TIMEOUT_SEAPILOT 5000 //en ms

// status word
#define ERROR_OK 0x0000 // ok
#define ERROR_BOTTOM_TRACK_LONG_LAG_PROCESS 0x0001 // si lors d'un suivi de fond il y a un trop gros retard dans le processus
#define ERROR_BOTTOM_TRACK_3_BEAM_SOLUTION 0x0002 // si le système utilise uniquement 3 insonorisations
#define ERROR_BOTTOM_TRACK_OLD 0x0004 // il y a un problème de détection du fond mais la valeur antérieur est encore valide
#define ERROR_BOTTOM_TRACK_SEACH 0x0008 // Si le suivi de fond change les paramètres de ping pour améliorer la détection du fond
#define ERROR_BOTTOM_TRACK_LONG_RANGE_NARROW 0x0010 // Indique que la la petite fréquence est utilisé pour la détection du fond à longue distance
#define ERROR_BOTTOM_TRACK_COAST 0x0020 // indique que le filtre est actif mais qu'il n'y a pas de nouvelle donnée valide en sortie
#define ERROR_BOTTOM_TRACK_PROOF 0x0040 // indique l'attente d'une nouvelle donnée valide pour établir une vitesse valide en sortie
#define ERROR_BOTTOM_TRACK_LOW_GAIN 0x0080 // indique que le gain a été réduit par rapport à au gain sélectionné
#define ERROR_HEADING_SENSOR 0x0100 // erreur sur le compas
#define ERROR_PRESSURE_SENSOR 0x0200 // erreur sur le capteur de pression
#define ERROR_POWER_DOWN_FAILURE 0x0400 // L'alimentation n'a pas été coupé entre deux ensembles de ping
#define ERROR_NON_VOLATILE_DATA 0x0800 // erreur de checksum en mémoire interne du DVL
#define ERROR_REAL_TIME_CLOCK 0x1000 // les données de l'horloge sont mauvaise 
#define ERROR_TEMPERATURE_SENSOR 0x2000 // Erreur sur le capteur de température, problème de communication ou la temperature n'est pas dans la bonne gamme (-30 à 70°C)
#define ERROR_RECEIVER_DATA 0x4000 // le nombre de donnée reçus n'est pas correcte
#define ERROR_RECEIVER_TIMEOUT 0x8000 // le hard n'a pas répondu à la requête de ping dans les temps


using namespace std;

// 192.168.1.102 1470

class SeaPilot : public BaseDriver{
public:
    
    /**
     * Constructeur
     */
    SeaPilot(string pathLogFile, bool enableLog = true);
    /**
     * Destructeur
     */
    virtual ~SeaPilot();
    /**
     * Permet d'afficher les commandes disponibles
     * @return 
     */
    bool help();
    
    /**
     * Démarrage du ping en continue
     * Une fois le système lancé seule les méthodes "stop()", "setRealTime(...)" et "showConfiguration()"
     * sont utilisables
     * @return 0 si ok
     */
    bool start();
    
    /**
     * Permet de lancer un unique ping
     * @return 0 si ok
     */
    bool startOneShot();
      
    /**
     * Stop du ping
     * @return 0 si ok
     */
    bool stop();
    /**
     * Permet de couper la puissance du capteur
     * @return 0 si ok
     */
    bool sleep();

    /**
     * Permet de la vitesse fond en X
     * @return la mitesse en mm/s
     */
    int getBottomTrackXVelocity() const;
    /**
     * Permet de la vitesse fond en Y
     * @return la mitesse en mm/s
     */
    int getBottomTrackYVelocity() const;
    /**
     * Permet de la vitesse fond en Z
     * @return la mitesse en mm/s
     */
    int getBottomTrackZVelocity() const;
        /**
     * Permet de la vitesse fond par rapport à l'est
     * @return la mitesse en mm/s
     */
    int getBottomTrackEastVelocity() const;
    /**
     * Permet de la vitesse fond par rapport au nord
     * @return la mitesse en mm/s
     */
    int getBottomTrackNorthVelocity() const;
    /**
     * Permet de la vitesse fond par rapport au haut
     * @return la mitesse en mm/s
     */
    int getBottomTrackUpVelocity() const;
    /**
     * Permet de retourner l'altitude
     * @return l'altitude en mm
     */
    int getDepthBottom() const;
    /**
     * Permet de retourner la profondeur
     * @return la profondeur en mm
     */
    int getDepthWaterMass() const;
    /**
     * Permet de retourner le numéro du ping
     * @return le numéro de ping
     */
    unsigned int getSampleNumber() const;
    /**
     * Permet de retourner la température
     * @return la température 
     */
    int getTemperature() const;
    /**
     * Permet de retourner l'heure du ping depuis le démarrage
     * @return heure du ping
     */
    unsigned int getTime() const;
    /**
     * Permet de retourner la vitesse du courant en X
     * @return vitesse du courant en mm/s
     */
    int getWaterMassXVelocity() const;
    /**
     * Permet de retourner la vitesse du courant en Y
     * @return vitesse du courant en mm/s
     */
    int getWaterMassYVelocity() const;
    /**
     * Permet de retourner la vitesse du courant en Z
     * @return vitesse du courant en mm/s
     */
    int getWaterMassZVelocity() const;
    /**
     * Permet de retourner la vitesse du courant par rapport à l'est
     * @return vitesse du courant en mm/s
     */
    int getWaterMassEastVelocity() const;
    /**
     * Permet de retourner la vitesse du courant par rapport au nord
     * @return vitesse du courant en mm/s
     */
    int getWaterMassNorthVelocity() const;
    /**
     * Permet de retourner la vitesse du courant par rapport au haut
     * @return vitesse du courant en mm/s
     */
    int getWaterMassUpVelocity() const;
    /**
     * Permet de retourner le roulis
     * @return le roulis en deg
     */
    float getRollUsedForWaterTracking()const;
    /**
     * Permet de retourner le tangage
     * @return le tangage en deg
     */
    float getPitchUsedForWaterTracking()const;
    /**
     * Permet de retourner le cap
     * @return le cap en deg
     */
    float getYawUsedForWaterTracking()const;
    
    /**
     * Permet de retourner le roulis utiliser lors du ping
     * @return le roulis en deg
     */
    float getRollUsedForBottomTracking()const;
    /**
     * Permet de retourner le tangage utiliser lors du ping
     * @return le tangage en deg
     */
    float getPitchUsedForBottomTracking()const;
    /**
     * Permet de retourner le cap utiliser lors du ping
     * @return le cap en deg
     */
    float getYawUsedForBottomTracking()const;
        /**
     * Permet de retourner le roulis
     * @return le roulis en rad
     */
    float getRollToRadUsedForWaterTracking()const;
    /**
     * Permet de retourner le tangage
     * @return le tangage en deg
     */
    float getPitchToRadUsedForWaterTracking()const;
    /**
     * Permet de retourner le cap
     * @return le cap en rad
     */
    float getYawToRadUsedForWaterTracking()const;
    
    /**
     * Permet de retourner le roulis utiliser lors du ping
     * @return le roulis en rad
     */
    float getRollToRadUsedForBottomTracking()const;
    /**
     * Permet de retourner le tangage utiliser lors du ping
     * @return le tangage en rad
     */
    float getPitchToRadUsedForBottomTracking()const;
    /**
     * Permet de retourner le cap utiliser lors du ping
     * @return le cap en rad
     */
    float getYawToRadUsedForBottomTracking()const;
    /**
     * Permet d'affecter la vitesse de sortie des données
     * @param h , heure
     * @param m , minute
     * @param s , seconde
     * @param ms , milliseconde
     */
    void setOutputRate(int h,int m, int s, int ms);
    
    /**
     * Commande CBTON (page 33)
     * Permet d'activer ou désactiver le bottom tracking
     * @param active true pour activer le bottom tracking
     */
    void setEnableBottomTrack(bool active);
    
    //Mode de fonctionnement du Bottom tracking
    enum BottomTrackControl_mode{
        NARROWBAND_LONG_RANGE = 0,// vitesse maxi de 11 m/s avec le faisceau à 20° et 7 m/s pour 30°
        BROADBAND_CODED_TRANSMIT = 1,// vitesse maxi de 13 m/s avec le faisceau à 20° et 9 m/s pour 30°
        BROADBAND_NON_CODED_TRANSMIT = 2,// vitesse maxi de 22 m/s avec le faisceau à 20° et 15 m/s pour 30°
        BROADBAND_NON_CODED_P_TO_P = 4, // la vitesse dépend du temps de retard de traitement      
        AUTO_SWITCH = 7 // Mode automatique qui switch entre les mode 0,2 et 4
    };
    
    //gestion des faisceaux
    enum BottomTrackControl_BeamMultiplex{
        BEAM_ONE_AT_A_TIME = 1,// ping et traitement : un faisceau par un faisceau
        BEAM_PAIRS = 2,// ping et traitement : par paire de faisceau
        BEAM_ALL = 4, // ping et traitement : pour les 4 faisceaux  
    };
    
    /**
     * Commande CBTBB (page 33)
     * Permet paramètrer le mode de fonctionnement du DVL
     * @param mode, mode de fonctionnement
     * @param pulseToPulse, Distance (m) avant de renvoyer un nouveau ping (distance fond x 2 (le X2 est pour l'aller/retour)). 
     * Permet de mesurer de faible variance de vitesse en faible fond. 
     * @param longRangeDepth, distance(m) lorsque le sytème switch vers le narrowband (longue distance) lorsque le système est dans le mode Auto (7) 
     * @param beamMultiplex, permet de sélectionner le nombre de faisceau à utiliser simultanément 
     */
    void setBottomTrackControl(BottomTrackControl_mode mode, double pulseToPulse, double longRangeDepth, BottomTrackControl_BeamMultiplex beamMultiplex);

    /**
     * Commande CBTST (page 34)
     * Permet de régler les seuils de prise en considération des données reçus par le DVL
     * @param correlationThreshold, Valeur entre 0.00 et 1.00 (defaut : 1.00) : Utilisé pour l'affichage des données du faisceau. 
     * Un faisceau d'une valeur inférieure au seuil de corrélation sera signalé mauvais et pas inclus dans la moyenne.
     * @param Q_VelocityThreshold, Valeur en m/s : Utilisé pour l'affichage des vitesses horizontales transformées. La vitesse est affiché si le Q absolue est supérieure au seuil.
     * @param V_VelocityThreshold, Valeur en m/s : Utilisé pour l'affichage des vitesses verticales transformées. La vitesse est affiché si le V absolue est supérieure au seuil.
     */
    void setBTSreeningThresholds(double correlationThreshold, double Q_VelocityThreshold , double V_VelocityThreshold);
    
    /**
     * Commande CBTT (page 34)
     * Permet de régler les seuils pour la gestion du rapport signal sur bruit suivant la profondeur
     * Abaisser les chiffres "SNR_shallow" et / ou "SNR_deep" permettra au DVL détecter de plus petit écho. La conséquence est que DVL peut détecter de faux écho lorsque le signal du fond est faible.
     * @param SNR_shallow , rapport signal sur bruit (dB) pour les faibles profondeurs. 
     * @param depth_shallow, profondeur où le DVL passe en détection faible fond avec le "SNR_shallow"
     * @param SNR_deep, rapport signal sur bruit (dB) pour les grandes profondeurs
     * @param depth_deep,  profondeur où le DVL switch de gain. Le ADCP / DVL a un émetteur de grande puissance. 
     * En eau peu profonde l'écho peut saturer l'entrée du récepteur, ce qui peut rendre difficile la détection du fond dans un environnement faible fond. 
     * Le ADCP / DVL met le récepteur à faible gain lorsque la profondeur est inférieure à la "depth_deep" . 
     * La variation du gain est d'environ 40 dB. Si vous observez que l'ADCP / DVL a des difficultés à détecter le fond près de la profondeur, il est nécessaire de régler "depth_deep" à une profondeur plus ou moins profonde selon la profondeur où la détection est faible.
     * 
     */
    void setBTThresholds(double SNR_shallow ,double depth_shallow, double SNR_deep ,double depth_deep);
    
    /**
     * Commande CBTBL (page 35)
     * Permet de définir la distance verticale où l'algorithme commence à rechercher le fond .
     * @param blank, distance entre 0.00 et 10.00(m)
     */
    void setBottomTrackBlank(double blank);
    
    /**
     * Commande CBTMX (page 35)
     * Permet de définir la valeur max où le DVL va chercher le fond
     * Plus la valeur est grande plus ça va prendre du temps pour récupérer les données
     * @param maxDepth, Profondeur max (m)(entre 5m et 10000m)
     */
    void setBottomTrackMaxDepth(double maxDepth);
    
    /**
     * Commande CBTTBP (page 35)
     * Permet de définir le temps entre chaque pings et cela indépendamment du mode. 
     * @param time, temps en seconde (entre 0.00s et 86400.00s (24h)).
     */
    void setBTTimeBetweenPings(double time);
    
    /**
     * Commande CBTFILT (page 35)
     * Permet de faire un filtrage passe-bas permettant de réduire le bruit de la mesure de vitesse.  
     * Le filtra permet d'avoir une vitesse de sortie moins sensible aux changements de vitesse (accélération).
     * Des constantes de temps longues produisent du bruit sur la vitesse. 
     * Le paramètre "ref_outlierThreshold" empêche les valeurs aberrantes de rentrer dans le filtre de sortie. 
     * Cependant, l'accélération rapide peut apparaître comme une série de valeurs aberrantes . 
     * Lorsque vous voulez vraiment ces valeurs de vitesse lié aux accélérations, il faut utiliser le Paramètre "ref_outlierCount" 
     * qui est utilisé pour détecter les accélérations (ou une série de valeurs aberrantes) 
     * ce qui permet au filtre de prendre comme référence la dernière mesure de la vitesse. 
     * Cela se produit lorsque le nombre de valeurs aberrantes consécutives correspond au paramètre de comptage des valeurs aberrantes "ref_outlierCount". 
     * Le filtre de référence sera normalement réglé à un temps plus lent que le filtre de sortie qui le rend mieux à même de détecter les valeurs aberrantes. 
     * Le paramètre "output_coastCount" permet à l'ADCP / DVL d'effacer tous les filtres lorsque les données de vitesse ne sont pas disponibles pendant une longue période de temps. 
     * Cela empêche que des données de vitesse plus âgés soit mélangés avec de nouvelles valeurs. 
     * Les deux filtres sont mis à jour une fois par ping (20 fois par seconde). 
     * 
     * @param output_timeConstant , temps pour le filtre passe bas de sortie (0.00 et 1.00) (defaut 0.5):
     * Une valeur de 0.00 désactive le filtre et la mesure de vitesse est transmise en directement. 
     * Une valeur de 1.00 empêche toute nouvelle donnée dans le filtre. 
     * Une valeur de 0.80 utilise 80% des données précédente en sortie du filtre et associé 20% des nouvelle valeur de vitesse.
     * @param ref_timeConstant, temps pour le filtre passe bas de référence (0.00 et 1.00) (defaut 0.95):
     * Une valeur de 0.00 désactive le filtre et la mesure de vitesse est transmise en directement. 
     * Une valeur de 1.00 empêche toute nouvelle donnée dans le filtre. 
     * Une valeur de 0.80 utilise 80% des données précédente en sortie du filtre et associé 20% des nouvelle valeur de vitesse.
     * @param ref_outlierCount , Nombre de points aberrant autorisé dans le fitre de référence (entre 0 et 9999) (défaut: 10) : Quand une nouvelle mesure de vitesse est acceptée, 
     * la variable de comptage de valeurs aberrantes interne est remis à 0 et le filtre de référence est réglé sur la mesure de vitesse en cours de validité.
     * @param ref_outlierThreshold, Vitesse limite avant de considérer que les valeurs sont aberrantes (valeur entre 0.00 et 1000.00m/s) (defaut: 1.0 m/s).
     * @param output_coastCount : Nombre de point invalid autorisé en sortie de filtre (entre 0 et 9999) (defaut : 30):
     * Si la nouvelle valeur de la vitesse est invalide le compteur interne est décrémenté. 
     * Lorsque le compteur atteint 0,le filtre de sortie est supprimée jusqu'à ce qu'une nouvelle mesure de vitesse valide se produit. 
     */
    void setBottomTrackFilter( double output_timeConstant, double ref_timeConstant , int ref_outlierCount , double ref_outlierThreshold, int output_coastCount);
   
    
    /**
     * Permet de régler l'horloge temps reel du DVL
     * @param yyyy , année
     * @param mm , mois
     * @param dd , jour
     * @param h , heures
     * @param m , minutes
     * @param s , secondes
     * @return true si ok
     */
    bool setRealTime(int yyyy , int mm , int dd, int h, int m, int s );
    
    /**
     * Commande CWTON (page 37)
     * Permet d'activer le suivi des courants entre deux distances (min et max)
     * @param enable , true pour activer
     * @param min_rangeProfiling , distance minimum du suivi (m)
     * @param max_rangeProfiling , distance minimum du suivi (m)
     */
    void setEnableWaterTracking(bool enable, double min_rangeProfiling, double max_rangeProfiling);
    
    /**
     * Commande CWTBB (page 37)
     * Permet d'activer le suivi des courants en faible fond
     * @param enable , true pour activer
     */
    void setEnableWTBroadband(bool enable);
    
    /**
     * Commande CWTBL (page 37)
     * Permet de définir la distance verticale où l'algorithme commence à faire le suivi .
     * @param blank, distance entre 0.00 et 100.00(m)
     */
    void setWTBlank(double blank);
    
    /**
     * Commande CWTBS (page 37)
     * Permet de définir la taille verticale du suivi
     * @param binSize, taille (m)
     */
    void setWTBinSize(double binSize);
    
    /**
     * Commande CWTTBP (page 37)
     * Permet de définir le temps entre chaque pings du water tracking
     * @param time, temps en seconde (entre 0.00s et 86400.00s (24h)).
     */
    void setWTTimeBetweenPings(double time);
    
    // commande pour l'établissement de la vitesse du son
    enum WaterSpeedOfSound_ctrl{
        COMMAND = 0, // utilisation des paramètres
        SENSOR = 1, // utilisation d'un capteur
        INTERNAL_CALCULATION = 2 // récupération par le calcul
    };
    /**
     * Commande CXSSC (page 37)
     * Permet de configurer les sources de données pour le calcule de la vitesse du son dans l'eau
     * @param waterTemperature_src , source pour la température de l'eau
     * @param transducerDepth_src , source pour la profondeur du transducteur
     * @param salinity_src, source pour la mesure de la salinité
     * @param speedOfSound_src, source pour la mesure du son dans l'eau
     */
    void setWaterSpeedOfSound(WaterSpeedOfSound_ctrl waterTemperature_src , WaterSpeedOfSound_ctrl transducerDepth_src , WaterSpeedOfSound_ctrl salinity_src , WaterSpeedOfSound_ctrl speedOfSound_src);

    /**
     * Commande CWS (page 37)
     * Permet de régler la salinité de l'eau en ppt
     * Eau salée : 35 ppt
     * Eau douce : 0 ppt
     * Estuaire : 15 ppt
     * @param ppt niveau de salinité
     */
    void setWaterSalinity(double ppt);
    
    /**
     * Commande CWT (page 37)
     * Permet de définir la temperature de l'eau.
     * La temperature de l'eau est utilisé dans le calcul de la vitesse du son dans l'eau, 
     * si le capteur de température n'est pas activé
     * @param WaterTemperature, temperature de l'eau (ex: 12.34)
     */
    void setWaterTemperature(double waterTemperature);
    
    /**
     * Commande CTD (page 37)
     * Permet de définir la profondeur du transducteur
     * La profondeur est utilisé pour le calcul de la vitesse du son dans l'eau
     * @param depth, profondeur du transducteur (m) (ex: 1.34)
     */
    void setTransducerDepth(double depth);
    
    /**
     * Commande CWSS (page 37)
     * Permet de définir la vitesse du son dans l'eau
     * Elle est utilisé quand la vitesse du son dans l'eau est défini en "COMMAND" ou en "SENSOR" (setWaterSpeedOfSound(...)).
     * @param waterSpeedOfSound, vitesse du son dans l'eau (m/s)
     */
    void setWaterSpeedOfSound(double waterSpeedOfSound);
    
    /**
     * Commande CPZ (page 38)
     * Permet de supprimer l'offset du capteur de pression
     */
    void setZeroPressureSensor();
    
    /**
     * Commande CLOAD (page 39)
     * Permet de charger le fichier "SYSCONF.BIN" contenu sur la carte SD du capteur.
     */
    void loadConfiguration();
    
    /**
     * Commande CSAVE (page 39)
     * Permet de sauvegarder la configuration courante dans le fichier "SYSCONF.BIN" contenu sur la carte SD du capteur.
     */
    void saveConfiguration();
    
    /**
     * Commande CDEFAULT (page 40)
     * Permet de recharger les réglages par défaut du capteur
     * Cette méthode doit être lancé pendant les 10 premières secondes du boot dans le cas d'une mauvaise configuration
     */
    void loadDefaultConfiguration();
    
    /**
     * Commande CSHOW (page 39)
     * Permet de récupérer les informations de configuration du capteur
     * @return les informations de la configuration sous la forme d'une chaine de caractère
     */ 
    string showConfiguration();
    
    
    /**
     * Permet de retourner les status retourner par le DVL
     *
     * @param statusActived , pointeur sur la liste des status retournés 
     * 
     * Si dessous les erreurs possible :
     * // status word
     * ERROR_OK 0x0000 // ok
     * ERROR_BOTTOM_TRACK_LONG_LAG_PROCESS 0x0001 // si lors d'un suivi de fond il y a un trop gros retard dans le processus
     * ERROR_BOTTOM_TRACK_3_BEAM_SOLUTION 0x0002 // si le système utilise uniquement 3 insonorisations
     * ERROR_BOTTOM_TRACK_OLD 0x0004 // il y a un problème de détection du fond mais la valeur antérieur est encore valide
     * ERROR_BOTTOM_TRACK_SEACH 0x0008 // Si le suivi de fond change les paramètres de ping pour améliorer la détection du fond
     * ERROR_BOTTOM_TRACK_LONG_RANGE_NARROW 0x0010 // Indique que la la petite fréquence est utilisé pour la détection du fond à longue distance
     * ERROR_BOTTOM_TRACK_COAST 0x0020 // indique que le filtre est actif mais qu'il n'y a pas de nouvelle donnée valide en sortie
     * ERROR_BOTTOM_TRACK_PROOF 0x0040 // indique l'attente d'une nouvelle donnée valide pour établir une vitesse valide en sortie
     * ERROR_BOTTOM_TRACK_LOW_GAIN 0x0080 // indique que le gain a été réduit par rapport à au gain sélectionné
     * ERROR_HEADING_SENSOR 0x0100 // erreur sur le compas
     * ERROR_PRESSURE_SENSOR 0x0200 // erreur sur le capteur de pression
     * ERROR_POWER_DOWN_FAILURE 0x0400 // L'alimentation n'a pas été coupé entre deux ensembles de ping
     * ERROR_NON_VOLATILE_DATA 0x0800 // erreur de checksum en mémoire interne du DVL
     * ERROR_REAL_TIME_CLOCK 0x1000 // les données de l'horloge sont mauvaise 
     * ERROR_TEMPERATURE_SENSOR 0x2000 // Erreur sur le capteur de température, problème de communication ou la temperature n'est pas dans la bonne gamme (-30 à 70°C)
     * ERROR_RECEIVER_DATA 0x4000 // le nombre de donnée reçus n'est pas correcte
     * ERROR_RECEIVER_TIMEOUT 0x8000 // le hard n'a pas répondu à la requête de ping dans les temps
     * 
     * @param size , le nombre de status retourner par le DVL
     */
    void getStatusDVL(int * statusActived , int &size);
    
    
    /**
     * Permet de retourner les dernières données du DVL en chaine de caractères
     * @return les dernières données DVL
     * Format csv :
     * timestamp; N° échantillon ; Température ; vitesse fond X ; vitesse fond Y ; vitesse fond Z ; altitude ; vitesse eau X ; vitesse eau Y ; vitesse eau Z ; profondeur ; status ; cap (bottom tracking) ; roulis (bottom tracking); tangage (bottom tracking) ; cap (water mass tracking) ; roulis (water mass tracking); tangage (water mass tracking)
     */
    string getDatas();
    
    /**
     * Permet de retourner les données cap, roulis, tangage 
     * @return H = yaw, P = pitch , R = Roll
     */
    string diagnoticCompass();
    
    /**
     * Structure contenant les informations de la configuration renvoyé par CSHOW
     */
    struct dvlConfiguration{
        string mode;//mode de fonctionnement
        struct {// temps entre les pings
            int hour;// heure
            int minute;//minute
            int seconde;//seconde
            int ms; // 10eme de milliseconde
        }cei;
        bool cbton; // activation du bottom track
        struct {// bottom track control
            int mode; // mode de fonctionnement 0 = narrowband, 1 = broadband coded , 2 = broadband non-coded , 4 = broadband pulse to pulse, 7 = auto
            double p2pLag; // distance de retard en mètre lorsque que le pulse to pulse est activé
            double longRangeDepth;//  distance où le bottom track passe en mode narrowband lorsque le système est en mode auto
            int beamMultiplex; // nombre de beam utilisé 1 = 1 à la fois , 2 = par pair , 4 = tous les beams
        }cbtbb;
        struct {// thresholds pour le bottom track
            double correlationThreshold; // valeur entre 0 et 1 , si la donnée du faisceau est inférieur au correlationThreshold alors n'est pas utilisée
            double qVelocityThreshold; // vitesse max dans la plan horizontal avant d'être considéré mauvaise
            double vVelocityThreshold; // vitesse max dans la plan vertical avant d'être considéré mauvaise
        }cbtst;
        double cbtbl;// défini la zone morte sous le capteur
        double cbtmx;// profondeur max du bottom track
        double cbttbp; //temps entre les pings
        struct {//bottom track threshold
            double shallowDetectionThreshold;//rapport signal sur bruit pour le faible fond
            double depthForShallowDetection; // profondeur à partir de laquelle on passe en faible fond
            double deepDetectionThreshold;// rapport signal sur bruit pour le grand fond
            double depth;//profondeur où l'on ajoute 40 db de gain
        }cbtt;
        struct { // filtre qui combine les deux fréquences du système
            double outputLowPassFilter;//le pourcentage de valeur filtré (0 si désactivé)
            double refLowPassFilter;// 
            double refOutlierCount;// nombre de outlier autorisé
            double refOutlierThreshold;// valeur avant de considérer que c'est un outlier
            double outputCoastCount; // nombre de valeur invalide autorisé
        }cbtfilt;
        struct {
            bool enable;// activation du water track
            double minRange; // valeur minimum de la portée
            double maxRange; // valeur max de la portée
        }cwton;
        bool cwtbb;//activation du water track broadband
        double cwtbl;//zone morte pour le water track
        double cwtbs;//taille de la colonne d'eau à tracker
        double cwttbp;//temps entre les pings du water track
        struct {//source des capteurs environnementaux : 0 = command , 1 = sensor, 2 = internal calculation
            int waterTemperatureSrc; // capteur de temperature de l'eau
            int transducerDepthSrc;// capteurde  profondeur
            int salinitySrc;// capteur de salinité
            int speedOfSoundSrc;//hydrophone
        }cwssc;
        double cws;//niveau de salinity 0ppt = eau douce , 15ppt = estuaire , 35ppt = mer
        double cwt;//temperature de l'eau
        double ctd; // profondeur du transducteur
        double cwss; // vitesse du son dans l'eau

    };
    /**
     * Permet de retourner la configuration courante du DVL
     * @return la structure contenant la configuration
     */
    dvlConfiguration getConfiguration();
    
    /**
     * Permet de savoir si les données du bottom tracking sont valides (pas de données à -99999)
     * @return true si valide
     */
    bool bottomTrackingIsValid();
    
    /**
     * Permet de savoir si les données du water tracking sont valides (pas de données à -99999)
     * @return true si valide
     */
    bool waterTrackingIsValid();
    
    /**
     * Nombre de données invalide consécutive lorsque que le bottom tracking courant est invalide 
     * @return nombre de données invalide
     */
    unsigned int numberOfBottomTrackInvalid();
    
    /**
     * Nombre de données invalide consécutive lorsque que le water tracking courant est invalide 
     * @return nombre de données invalide
     */
    unsigned int numberOfWaterTrackInvalid();
    
    /**
     * Permet de retourner le delta de temps entre deux données
     * @return delta de temps en seconde
     */
    float getDt();
    

protected:
    
    /**
     * Callback permettant d'attendre les caractères et ainsi créer la trame 
     * afin de la parser dans readOnSensor() ou readOnFile();
     * @return la trame à parser
     */
    string waitDatas();
    /**
     * Callback pour parser les données venant d'une communication autre que d'un fichier
     * @param trame, la trame à parser
     * @return 0 si ok 
     */
    int readOnSensor(string trame);
    /**
     * Callback pour parser les données venant d'un fichier de log
     * @param trame, la trame à parser
     * @return 0 si ok 
     */
    int readOnFile(string trame);

    void timeout(){};


private:
    char * numberToArray(int number);
    /**
     * Permet de calculer le checksum de la trame reçu
     * @param buf, trame
     * @return le checksum
     */
    int calculateChecksum(const char* buf, int size);
    
    

    unsigned int _time;
    unsigned int _sampleNumber;
    
    // water temperature
    int _temperature;
    
    // pressure
    int _pressure;
    
    //bottom speed
    int _bottomTrackXVelocity;
    int _bottomTrackYVelocity;
    int _bottomTrackZVelocity;
    int _bottomTrackEastVelocity;
    int _bottomTrackNorthVelocity;
    int _bottomTrackUpVelocity;
    //distance depth
    int _depthBottom;
    
    //water speed
    int _waterMassXVelocity;
    int _waterMassYVelocity;
    int _waterMassZVelocity;
    int _waterMassEastVelocity;
    int _waterMassNorthVelocity;
    int _waterMassUpVelocity;
    //distance water
    int _depthWaterMass;
    
    //attitude for the water tracking
    float _rollUsedForWaterTracking;
    float _pitchUsedForWaterTracking;
    float _yawUsedForWaterTracking;
    
    // attittude dor the bottom tracking
    float _rollUsedForBottomTracking;
    float _pitchUsedForBottomTracking;
    float _yawUsedForBottomTracking;
    
    //bit status en hexa 0000 = Ok
    int _status;
    
    //flag de validité
    bool _btIsValid;// bottom track
    bool _wtIsValid;// water track
   
    // compteur de données invalides consécutives
    unsigned int _counterInvalidBT;
    unsigned int _counterInvalidWT;
    
    float _dt;
    
    bool _sync;
    
    
};

#endif	/* __SEAPILOT_H_ */
