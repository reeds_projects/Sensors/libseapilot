from conans import ConanFile, CMake, tools


class LibseapilotConan(ConanFile):
    name = "libSeaPilot"
    version = "1.3"
    license = "CECILL"
    author = "Benoit ROPARS benoit.ropars@solutionsreeds.fr"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = ["src/*","include/*","examples/*","CMakeLists.txt"]
    _source_dir = "{}-{}".format(name, version)
    requires = "libDrivers/2.7.3@reeds_projects+Sensors+libdrivers/release"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*", src="bin", dst="bin")
        self.copy("*.h", dst="include", src= "include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["SeaPilot"]
