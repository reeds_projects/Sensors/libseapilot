
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include "SeaPilot.h"

#include <signal.h>

SeaPilot *seaPilot;

bool sortie = false;

using namespace std;

void sighandler(int sig)
{
    sortie = true;

}



//int main(int argc, char** argv) {
//    // SeaPilot.
//    seaPilot = new SeaPilot("log.txt");
//    
//    
//	
//    // Connection.
//    cout << "connected:" << seaPilot->connect("192.168.1.102:1470")<< endl;
//    
//   seaPilot->setOutputRate(0,0,0,10);
////    seaPilot->stop();
////    sleep(1);
//    cout << "start :" << seaPilot->start() << endl;
//    
//    sleep(1);
//    
//    //seaPilot->sleep();
//    //cout << "start :" <<seaPilot->help() << endl;
//    
//    //while(1);
//    
//    while (1) {
//        seaPilot->read();
//        
//        cout << seaPilot->getBottomTrackXVelocity()<< " , "<<
//        seaPilot->getBottomTrackYVelocity()<< " , "<<
//        seaPilot->getBottomTrackZVelocity()<< " , "<<
//        seaPilot->getDepthBottom()<< " , "<<
//        seaPilot->getDepthWaterMass()<< " , "<<
//        seaPilot->getSampleNumber()<< " , "<<
//        seaPilot->getTemperature()<< " , "<<
//        seaPilot->getTime()<< " , "<<
//        seaPilot->getWaterMassXVelocity()<< " , " <<
//        seaPilot->getWaterMassYVelocity()<< " , "<< 
//        seaPilot->getWaterMassZVelocity()<< endl;
//        
//        cout << seaPilot->getYawUsedForBottomTracking()<< " , "<<
//        seaPilot->getPitchUsedForBottomTracking()<< " , "<<
//        seaPilot->getRollUsedForBottomTracking()<< endl;
//        
//        cout << seaPilot->getYawUsedForWaterTracking()<< " , "<<
//        seaPilot->getPitchUsedForWaterTracking()<< " , "<<
//        seaPilot->getRollUsedForWaterTracking()<< endl;
//        
//    }
//
//    return 0;
//}
//




int main(int argc, char** argv) {
    
//    signal(SIGABRT, &sighandler);
//    signal(SIGTERM, &sighandler);
//    signal(SIGINT, &sighandler);
//    
//    // SeaPilot.
//    seaPilot = new SeaPilot("log.txt" , true);
//    
//    // permet de stocker les status renvoyé par le DVL
//    int status[16];
//    int size = 0;
//
//
//   cout << seaPilot->connect("192.168.1.102:1470") << endl;
//   cout << seaPilot->showConfiguration() << endl;
//   
////   seaPilot->start();
//   
//   
//   while ( sortie != true){
////   cout << seaPilot->diagnoticCompass() << endl;
////       seaPilot->read();
////        
//// //sleep(1);
////        
////        cout << seaPilot->getBottomTrackXVelocity()<< " , "<<
////        seaPilot->getBottomTrackYVelocity()<< " , "<<
////        seaPilot->getBottomTrackZVelocity()<< " , "<<
////        seaPilot->getDepthBottom()<< " , "<<
////        seaPilot->getDepthWaterMass()<< " , "<<
////        seaPilot->getSampleNumber()<< " , "<<
////        seaPilot->getTemperature()<< " , "<<
////        seaPilot->getTime()<< " , "<<
////        seaPilot->getWaterMassXVelocity()<< " , " <<
////        seaPilot->getWaterMassYVelocity()<< " , "<< 
////        seaPilot->getWaterMassZVelocity()<< endl;
//    
////        seaPilot->getStatusDVL(status,size);
////        for(int i = 0 ; i < size;i++)cout <<"status : " << std::hex << status[i]<< endl;
//        
////        cout << seaPilot->getYawUsedForBottomTracking() << endl;
//       
//       
//        cout << seaPilot->getConfiguration().cbtbb.longRangeDepth<< endl;
//        
//
//   }
//
//   cout << "STOP application" << endl;
//   seaPilot->stop();
//   
//   sleep(1);
//    
//    delete seaPilot;
//    return 0;
//########################## TEST de la lecture d'un fichier de log
    signal(SIGABRT, &sighandler);
    signal(SIGTERM, &sighandler);
    signal(SIGINT, &sighandler);

    // SeaPilot.
    seaPilot = new SeaPilot("log" , true);
   
   cout << seaPilot->connectTCP("192.168.1.102:1470") << endl;
   //cout << seaPilot->connectFile("datas_seapilot.txt:10") << endl;
   
   cout << seaPilot->showConfiguration() << endl;
   
   seaPilot->start();
   
    // permet de stocker les status renvoyé par le DVL
    int status[16];
    int size = 0;
   
   
   while ( sortie != true){
        seaPilot->read();
       
        cout << seaPilot->getBottomTrackXVelocity()<< " , "<<
        seaPilot->getBottomTrackYVelocity()<< " , "<<
        seaPilot->getBottomTrackZVelocity()<< " , "<<
        seaPilot->getDepthBottom()<< " , "<<
        seaPilot->getDepthWaterMass()<< " , "<<
        seaPilot->getSampleNumber()<< " , "<<
        seaPilot->getTemperature()<< " , "<<
        seaPilot->getTime()<< " , "<<
        seaPilot->getWaterMassXVelocity()<< " , " <<
        seaPilot->getWaterMassYVelocity()<< " , "<< 
        seaPilot->getWaterMassZVelocity()<< endl;

        seaPilot->getStatusDVL(status,size);
        for(int i = 0 ; i < size;i++)cout <<"status : " << std::hex << status[i]<< std::dec<<endl;


   }

    delete seaPilot;
    return 0;    
    
}
