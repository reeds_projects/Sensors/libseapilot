## LICENCE

Cecill licensed code

Company: REEDS

Author: ROPARS Benoît (benoit.ropars@solutionsreeds.fr)

All codes are made available free of charge. These are codes from the manufacturer's documentation.
Libraries using this code are available under a proprietary license.We can also develop specific solutions for your needs. You can contact us via the following address: contact@solutionsreeds.fr

## CONAN PACKAGE

[![pipeline status](https://gitlab.com/reeds_projects/Sensors/libseapilot/badges/1.3/pipeline.svg)](https://gitlab.com/reeds_projects/Sensors/libseapilot/-/commits/1.3)


## CONAN INSTALL PACKAGE

    $ mkdir build && cd build
    $ conan install ..


## MODIFICATION

- v.1.3(14/01/2017) : Version Expérimentation Navscoot N°2
- v.1.2 : Correction bug de compilation sous linux
- v.1.1 : Nettoyage du code
- v.1.0 : First version
